import math


#länge der welle in m
l = 0.75
#abstand der scheiben in m (ist differenz der lagerungen)
s = 0.5 - 0.25
#emodul der welle in N/m**2
E = 210000*10**6
#durchmesser der Welle in m
d = 0.008
#masse deiner Scheibe in kg
m = 0.5


#y
y = s/l
#a_1 und a_2
a_1 = 1 - 2*y*y + y*y*y*y
a_2 = 1 - 4*y*y + 4*y*y*y - y*y*y*y
#flächenträgheiitsmoment berech
I = math.pi*d*d*d*d/64
#ersatzfedersteifigkeit k berechnen in N/m
k = 48*E*I/(l*l*l)

#kritische kreisfrequenzen in 1/min
w_1 = math.sqrt(k/(m*(a_1 + a_2)))*60/(2*math.pi)
w_2 = math.sqrt(k/(m*(a_1 - a_2)))*60/(2*math.pi)

print(w_1)
print(w_2)